/**
 * Created with PhpStorm.
 * User: LiuYang
 * Date: 2014/11/22
 * Time: 16:21
 * Description:数据库模块
 */
var mysql = require('mysql');
var client = mysql.createConnection({
    'host':'localhost',
    'user': 'root',
    'password': '',
    'database':'docs'
});
module.exports = client;