# README #

基于AngularJs实现的在线帮助系统

### 开发帮助系统的目的及意义? ###

* 帮助开发者快速找到自己想要的内容
* Version 1.0
* [项目地址](https://lywzx@bitbucket.org/lywzx/doc.git)

### 如何在此基础上继续开发? ###

* 从bithub上迁出项目到指定位置
* 安装NODE.JS开发环境，以方便模拟假数据
* 执行npm install，安装NODE.JS依赖
* 编译less文件，生成对应的CSS文件
* 执行node bin/www命令启动项目

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact