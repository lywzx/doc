/**
 * Created with PhpStorm.
 * User: Liu Yang
 * Date: 14-11-4
 * Time: 下午6:00
 * Description:  将要输出的内容直接转化为HTML
 */
Docs.filter('toHtml', ['$sce',function($sce){
    return function(val){
        return $sce.trustAsHtml(val);
    }
}]);
