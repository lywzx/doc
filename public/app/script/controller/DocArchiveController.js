/**
 * Created with PhpStorm.
 * User: Liu Yang
 * Date: 14-10-11
 * Time: 下午1:46
 * Description: 内容详情控制器
 */
Docs.controller('DocArchiveController',['$scope','$routeParams','$location','$http','$timeout','content',function($scope,$routeParams,$location,$http,$timeout,content){

    //绑定数据到视图
    $scope.content = content.data.content;
    //更新面包屑导航
    $scope.$emit('updateCrumbs',content.data.crumbs);
    //代码高亮
    $timeout(function(){
        window.prettyPrint && window.prettyPrint();
    });
}]);