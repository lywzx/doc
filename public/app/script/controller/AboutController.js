/**
 * Created with PhpStorm.
 * User: Liu Yang
 * Date: 14-11-5
 * Time: 上午9:17
 * Description:
 */
Docs.controller('AboutController',['$scope','archive','$routeParams', 'docsData',function($scope, archive,$routeParams,docsData){
    var request = {
        type: $routeParams.type,
        pselect:'about'
    }
    archive.get(request,function(d){
        if(d.statusCode === 200) {
            //关闭加载的按钮
            docsData.isLoading = false;
            //视图信息
            $scope.about = d.data;
            //更新当前位置信息
            $scope.$emit('updateCrumbs', d.data.crumbs);
        } else {
            //失败的情况
        }
    });
}]);