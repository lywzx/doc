/**
 * Created with PhpStorm.
 * User: Liu Yang
 * Date: 14-9-30
 * Time: 下午2:16
 * Description:列表控制器
 */
Docs.controller('RootController',['$scope','$timeout','$location','docsData','archive',function($scope,$timeout,$location,docsData,archive){
    //应用配置参数
    $scope.init = {
        "isSearch": false,
        'd': docsData
    };

    $scope.searchBlur = function(){
        $timeout(function(){
            $scope.init.isSearch = false;
        },300);
    };

    //加载右侧菜单
    archive.get({type:$location.$$url.split('/')[1].replace('.html',"")},function(d){
        if(d.statusCode === 200) {
            //更新右侧菜单
            $scope.$emit('updatesideMenu', d.data.menu);
            docsData.title = d.data.title;
        } else {
            //。。。
        }
    });
}]);
