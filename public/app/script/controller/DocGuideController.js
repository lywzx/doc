/**
 * Created with PhpStorm.
 * User: LiuYangX
 * Date: 14-10-8
 * Time: 下午2:42
 * Description: 内容引导控制器
 */

Docs.controller('DocGuideController',['$scope','docGuide',function($scope,docGuide){
    $scope.content = docGuide.data.content;
    //更新对应的位置导航信息
    $scope.$emit('updateCrumbs',docGuide.data.crumbs);
}]);