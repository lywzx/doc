var Docs = angular.module('Docs',['ngRoute','ngAnimate','Docs.services']);
Docs.config(['$routeProvider','$locationProvider',function($routeProvider, $locationProvider){
    //开启HTML 5路由模式
    $locationProvider.html5Mode(true);
    //配置路由
    $routeProvider.
        when('/',{
            'templateUrl': 'app/views/aboutDoc.html',
            'controller': 'AboutController'
        }).
        when('/:type',{ // 如/Docs/JQuery-1.83
            'templateUrl': 'app/views/aboutDoc.html',
            'controller': 'AboutController'
        }).
        when('/:type/:pselect',{ //如/Docs/JQuery-1.83/selector
            'templateUrl': 'app/views/docGuide.html',
            'controller': 'DocGuideController',
            'resolve': {
                'docGuide': ['archiveLoader',function(archiveLoader){
                    return archiveLoader();
                }]
            }
        })
        .when('/:type/:pselect/:id',{
            'templateUrl': 'app/views/docArchive.html',
            'controller': 'DocArchiveController',
            'resolve': {
                'content':['archiveLoader',function(archiveLoader){
                    return archiveLoader();
                }]
            }
        })
        .otherwise({
            'redirect': '/Docs/jQuery'
        });
}]);

Docs.run(['$rootScope', '$window', '$location', '$log', '$routeParams','docsData', function($rootScope, $window, $location, $log, $routeParams, docsData) {
    var locationChangeStartOff = $rootScope.$on('$locationChangeStart', locationChangeStart);
    var locationChangeSuccessOff = $rootScope.$on('$locationChangeSuccess', locationChangeSuccess);

    var routeChangeStartOff = $rootScope.$on('$routeChangeStart', routeChangeStart);
    var routeChangeSuccessOff = $rootScope.$on('$routeChangeSuccess', routeChangeSuccess);

    //更新页包屑导航
    $rootScope.$on('updateCrumbs',function(e,d){
        docsData.crumbs.length = 0;
        angular.extend(docsData.crumbs, d);
    });

    //更新右侧展开折叠导航
    $rootScope.$on('updatesideMenu', function(e, d){
        docsData.sideMenuData.length = 0;
        angular.extend(docsData.sideMenuData, d);
    });

    function locationChangeStart(event) {
        //显示加载按钮
        docsData.isLoading = true;
    }

    function locationChangeSuccess(event) {
        //$log.log('locationChangeSuccess');
        //$log.log(arguments);
    }

    function routeChangeStart(event) {
        //$log.log('routeChangeStart');
        //$log.log(arguments);
    }

    function routeChangeSuccess(event) {
        //$log.log('routeChangeSuccess');
        //$log.log(arguments);
        var type = $routeParams.type || "",pselect = $routeParams.pselect || "",id = $routeParams.id || "";
        type = type.replace('.html',"");
        pselect = pselect.replace('.html',"");
        id = id.replace('.html',"");
        docsData.type = type;
        docsData.pselect = pselect;
        docsData.id = id;
        docsData.isLoading = false;
    }
}]);