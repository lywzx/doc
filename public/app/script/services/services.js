/**
 * Created with PhpStorm.
 * User: LiuYang
 * Date: 14-10-4
 * Time: 下午6:47
 * Description:
 */

var services = angular.module('Docs.services',['ngResource']);

services.value('docsData', {
    "crumbs":[],
    "sideMenuData": [],
    "type":"",
    "pselect":"",
    "id":"",
    "title": "",
    "isLoading": false
});

services.factory('archive',['$resource',function($resource){
    return $resource('/Ajax/:type/:pselect/:id',{type: '@type',pselect:'@pselect','id':'@id'});
}]);

services.factory('archiveLoader',['archive','$route','$q',function(archive,$route,$q){
    return function(){
        var delay = $q.defer(),sendparam={},route;
        route = $route.current.params;
        angular.forEach(route, function(i,j){
            sendparam[j] = i.replace('.html', '');
        });
        archive.get(sendparam,function(content){
            delay.resolve(content);
        },function(){
            delay.reject('Unable to fetch recipe '  + $route.current.params.id);
        });
        return delay.promise;
    };
}]);