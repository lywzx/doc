/**
 * Created with PhpStorm.
 * User: Liu Yang
 * Date: 14-11-5
 * Time: 上午11:38
 * Description:
 */
//面包斜导航
Docs.directive('crumbs',['docsData', function(docsData){
    var template = "<span>当前位置：</span><a href='{{ item.href }}' ng-repeat-start='item in crumbs'>{{ item.title }}</a><span ng-repeat-end>&gt;&gt;</span>";

    return {
        'restrict': 'A',
        'template': template,
        'scope': true,
        'link':function(scope){
            scope.crumbs = docsData.crumbs;
        }
    };
}]);

//侧边菜单导航
Docs.directive('sideMenu',['docsData',function(docsData){
    var template = "<menu class='menu-nav'>\
            <li ng-repeat='menu_item in m' ng-class='{open:d.pselect == menu_item.rename,selected:d.pselect == menu_item.rename}'>\
                <i ng-class=\"{true:'icon-folder-open' ,false:'icon-folder-empty'}[pselect == menu_item.rename]\"></i>\
                <a ng-href='{{ d.type }}/{{ menu_item.rename }}.html' title='{{menu_item.title}}'>{{menu_item.title}}</a>\
            <ul class='menu-nav-1'>\
                <li ng-repeat='menu2_item in menu_item.child' ng-class='{selected:d.id == menu2_item.rename}'>\
                    <i class='icon-doc'></i>\
                    <a ng-href='{{ d.type }}/{{menu_item.rename}}/{{ menu2_item.rename }}.html' title='{{menu2_item.title}}'>{{menu2_item.title}}</a>\
                </li>\
            </ul>\
        </li>\
    </menu>";
    return {
        'restrict': 'A',
        'template':template,
        'scope':true,
        'link':function(scope){
            scope.d = docsData;
            scope.m = docsData.sideMenuData;
        }
    }
}]);

//搜索弹层
Docs.directive('searchLayer', ['docsData', function(docsData){
    var template = '<div class="pos">\
            <h3>属性搜索结果列表</h3>\
        </div>\
        <div class="search-container pdH20 clearfix">\
            <dl class="fl search-item" ng-repeat=\'item in filtered = ( m | filter:keyword | orderBy:"child.length":false)\'>\
                <dt class="search-item-title"><a href="{{ d.type }}/{{ item.rename }}.html">{{ item.title }}</a></dt>\
                <dd>\
                    <ul>\
                        <li ng-repeat="i in item.child | filter:keyword">\
                            <a href="{{ d.type }}/{{item.rename}}/{{ i.rename }}.html">{{ i.title }}</a>\
                        </li>\
                    </ul>\
                </dd>\
            </dl>\
            <span ng-hide="filtered.length">你想找的信息木有哦，不要恢心哦！</span>\
        </div>';
    return {
        'restrict': 'A',
        'template': template,
        'scope': {
            'keyword':'='
        },
        'link':function(scope){
            scope.d = docsData;
            scope.m = docsData.sideMenuData;
        }
    }
}]);