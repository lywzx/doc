var express = require('express');
var router = express.Router();
var http = require('http');
var BufferHelper = require('bufferhelper');

/* GET home page. */
router.get('/jQuery-1.83/*', function(req, res) {
    res.render('index', { title: 'Express' });
});

function getSQL(dataSource,res) {
    http.get({
        host: 'www.css119.com',
        port: 80,
        path: dataSource.href
    }, function(respond){
        //res.send(respond);
        var bufferHelper = new BufferHelper();
        respond.on('data', function(chunk){
            bufferHelper.concat(chunk);
        });
        respond.on('end',function(){
            var html = bufferHelper.toBuffer().toString();
            html = html.replace(/(^\s+)|(\s+$)|[\n\f]/gm,"");
            console.log(html);
            html = html.match(/<body id="split">(.*)<\/body>/)[1];
            console.log(html);
            html = html.replace(/<pre><code>/g, '<pre class="prettyprint linenums">');
            html = html.replace(/<\/code><\/pre>/g,"</pre>");
            var archive = html.match(/<div id="content"><div rel="jQuery">(.*)<\/div><\/div>/)[1];
            var reg = /<h2><span>返回值:(.*)<\/span>(.*)<\/h2>/;
            var titleRet = reg.exec(archive);
            archive = archive.replace(reg, "");
            var data = {
                "title": titleRet[2],
                "rename":dataSource.href.replace("/book/jQuery/",""),
                "supTip":dataSource.supTip,
                "isDelete":'0',
                "returnVal": titleRet[1],
                "summary":archive.match(/<div class="desc">(.*)<\/div><h3>/)[1],
                "archive": archive,
                "createTime":Date.now()
            };
            var sql = "INSERT INTO `ly_archive` (`id`, `title`, `rename`, `keywords`, `supTip`, `isDelete`, `summary`, `archive`, `returnVal`, `createTime`, `pid`, `uid`, `did`, `aid`) VALUES (NULL, '"+ data.title
                +"', '" + data.rename +
                "', '','" + data.supTip +"', '"+ data.isDelete +"' , '"+ data.summary +"', '" + data.archive+ "', '"+ data.returnVal +"', '" +data.createTime+ "', 0, 1, 3, '')";
            console.log(sql);
            //加载数据库模块
            /*var client = require('../config/databases');
            client.connect();
            client.query('CREATE TABLE table_name(column_1 int NOT NULL);', function (err, result, fields) {
                console.log(111);
            });*/

            //res.writeHead(200);
            res.send(sql);
            //res.end(archive);
        });
    }).on('error', function(e) {
        console.log("Got error: " + e.message);
    });
}

//首页的关于手册的接口
router.get('/test/',function(req,res){
    var dataUrl = require('./Data');
    (function (d) {
        for(var i=0; i< d.length; i++){
            if(d[i].child && d[i].child.length > 0){
                arguments.callee(d[i]);
            }
            if(d[i].href) {
                getSQL(d[i],res);
            }
        }
    })([{
        title:"jQuery([sel,[context]])",
        href:"/book/jQuery/jQuery_selector_context.html",
        subTip:""
    }]);
});

module.exports = router;
