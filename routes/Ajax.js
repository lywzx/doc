/**
 * Created with PhpStorm.
 * User: Liu Yang
 * Date: 14-9-25
 * Time: 下午3:32
 * Description: 获取文单的内容
 */
var express = require('express');
var router = express.Router();

//关于手册的相关数据
router.get('/jQuery-1.83/about',function(req, res){
    res.json({
        "statusCode": 200,
        "message": "数据拉取成功",
        "data": {
            "title": "前言",
            "crumbs": [
                {"title": "首页","href": "/"},
                {"title": "jQuery", "href":"/jQuery-1.83"},
                {"title": "关于", "href": "/jQuery-1.83/about"}
            ],
            "author": "刘洋",
            "content": "<p>现今浏览器已经支持了众多的CSS3属性，但是大部分开发人员和设计师貌似依然在关注于一些很“主流”的属性，如border-radius、box-shadow或者transform等。它们有良好的文档、很好的测试并且最常用到，所以如果你最近在设计网站，你很难能脱离它们。</p><p>隐藏在浏览器的大宝库中是一些高级的、被严重低估的属性，但是它们并没有得到太多的关注。最伟大的财富隐藏在Webkit的下面，而且在iPhone、iPad和Android apps的时代，开始了解它们会非常有用。</p><p>以开发“结构更简单”、“性能更高效”、“功能更高效”的UI组件及项目，以及提升 WEB 开发效率，降低前端开发人员的开发及学习成本，了解并学习 webkit 内核属性，扩大知识面为目标，我们编写了本手册。</p><p>由于精力有限，手册内容难免出现错误和问题，望广大网友不吝斧正。请将您发现的问题提交到手册问题反馈，将会在鸣谢页面列出您的名字，以表达对您的感激。</p><p>现今浏览器已经支持了众多的CSS3属性，但是大部分开发人员和设计师貌似依然在关注于一些很“主流”的属性，如border-radius、box-shadow或者transform等。它们有良好的文档、很好的测试并且最常用到，所以如果你最近在设计网站，你很难能脱离它们。</p><p>隐藏在浏览器的大宝库中是一些高级的、被严重低估的属性，但是它们并没有得到太多的关注。最伟大的财富隐藏在Webkit的下面，而且在iPhone、iPad和Android apps的时代，开始了解它们会非常有用。</p><p>以开发“结构更简单”、“性能更高效”、“功能更高效”的UI组件及项目，以及提升 WEB 开发效率，降低前端开发人员的开发及学习成本，了解并学习 webkit 内核属性，扩大知识面为目标，我们编写了本手册。</p><p>由于精力有限，手册内容难免出现错误和问题，望广大网友不吝斧正。请将您发现的问题提交到手册问题反馈，将会在鸣谢页面列出您的名字，以表达对您的感激。</p><p>现今浏览器已经支持了众多的CSS3属性，但是大部分开发人员和设计师貌似依然在关注于一些很“主流”的属性，如border-radius、box-shadow或者transform等。它们有良好的文档、很好的测试并且最常用到，所以如果你最近在设计网站，你很难能脱离它们。</p><p>隐藏在浏览器的大宝库中是一些高级的、被严重低估的属性，但是它们并没有得到太多的关注。最伟大的财富隐藏在Webkit的下面，而且在iPhone、iPad和Android apps的时代，开始了解它们会非常有用。</p><p>以开发“结构更简单”、“性能更高效”、“功能更高效”的UI组件及项目，以及提升 WEB 开发效率，降低前端开发人员的开发及学习成本，了解并学习 webkit 内核属性，扩大知识面为目标，我们编写了本手册。</p><p>由于精力有限，手册内容难免出现错误和问题，望广大网友不吝斧正。请将您发现的问题提交到手册问题反馈，将会在鸣谢页面列出您的名字，以表达对您的感激。</p>"
        }
    });
});

/* 获取文章菜单相关 */
router.get('/jQuery-1.83',function(req, res){
    res.json({
        "statusCode": 200,
        "message": "请求成功",
        "data": {
            "id": "05",
            "title": "jQuery帮助手册",
            "menu": [
                {"id":"03","title":"速查表","rename":"quicksearch"},
                {"id":"04","title":"核心","rename": "core","child":[
                    {"id":"07","title":"jQuery(selector,[context])","rename":"jQuery_selector_context"},
                    {"id":"09","title":"jQuery(html,[ownerDocument])","rename": "jQuery_html_ownerDocument","supTip":"1.8*"},
                    {"id":"10","title":"jQuery(callback)","rename":"jQuery_callback"},
                    {"id":"11","title":"jQuery.holdReady(hold)","rename":"jQuery.holdReady","supTip": "1.6+"},
                    {"id":"12","title":"each(callback)","rename":"each_callback"},
                    {"id":"13","title":"size()","rename":"size"},
                    {"id":"14","title":"length","rename":"length"},
                    {"id":"15","title":"selector","rename":"selector"},
                    {"id":"16","title":"context","rename":"context"},
                    {"id":"17","title":"get([index])","rename":"get_index"},
                    {"id":"18",'title':"index([selector|element])","rename":"index_selector_element"},
                    {"id":"19","title":"data([key],[value])","rename":"data_key_value"},
                    {"id":"20","title":"removeData([name|list])"},
                    {"id":"21","title":"jQuery.data(element,[key],[value])"},
                    {"id":"22","title":"queue(element,[queueName])"},
                    {"id":"23","title":"dequeue([queueName])"},
                    {"id":"24","title":"clearQueue([queueName])"},
                    {"id":"25","title":"jQuery.noConflict([extreme])"}
                ]},
                {"id":"26","title":"选择器","rename":"jq.selector","child":[
                    {"id":"27","title":"#id"},
                    {"id":"28","title":"element"},
                    {"id":"29","title":".class"},
                    {"id":"30","title":"*"},
                    {"id":"31","title":"selector1,selector2,selectorN"},
                    {"id":"32","title":"ancestor descendant"},
                    {"id":"33","title":"parent > child"},
                    {"id":"34","title":"prev + next"},
                    {"id":"35","title":"prev ~ siblings"},
                    {"id":"36","title":":first"},
                    {"id":"37","title":":last"},
                    {"id":"38","title":":not(selector)"},
                    {"id":"39","title":":even"},
                    {"id":"40","title":":odd"},
                    {"id":"41","title":":eq(index)"},
                    {"id":"42","title":":gt(index)"},
                    {"id":"43","title":":lt(index))"},
                    {"id":"44","title":":header"},
                    {"id":"45","title":":animated"},
                    {"id":"46","title":":focus"},
                    {"id":"47","title":":contains(text)"},
                    {"id":"48","title":":empty"},
                    {"id":"49","title":":has(selector)"},
                    {"id":"50","title":":parent"},
                    {"id":"51","title":":hidden"},
                    {"id":"52","title":":visible"},
                    {"id":"53","title":"[attribute]"},
                    {"id":"54","title":"[attribute=value]"},
                    {"id":"55","title":"[attribute!=value]"},
                    {"id":"56","title":"[attribute^=value]"},
                    {"id":"57","title":"[attribute$=value]"},
                    {"id":"58","title":"[attribute*=value]"},
                    {"id":"59","title":"[selector1][selector2][selectorN]"},
                    {"id":"60","title":":nth-child"},
                    {"id":"61","title":":first-child"},
                    {"id":"62","title":":last-child"},
                    {"id":"63","title":":only-child"},
                    {"id":"64","title":":input"},
                    {"id":"65","title":":text"},
                    {"id":"66","title":":password"},
                    {"id":"67","title":":radio"},
                    {"id":"68","title":":checkbox"},
                    {"id":"69","title":":submit"},
                    {"id":"70","title":":image"},
                    {"id":"71","title":":reset"},
                    {"id":"72","title":":button"},
                    {"id":"73","title":":file"},
                    {"id":"74","title":":hidden"},
                    {"id":"75","title":":enabled"},
                    {"id":"76","title":":disabled"},
                    {"id":"77","title":":checked"},
                    {"id":"78","title":":selected"}
                ]},
                {"id":"79","title":"属性","rename":"prototype","child":[
                    {"id":"80","title":"attr(name|pro|key,val|fn)"},
                    {"id":"81","title":"removeAttr(name)"},
                    {"id":"82","title":"prop(name|pro|key,val|fn)"},
                    {"id":"83","title":"removeProp(name)"},
                    {"id":"84","title":"addClass(class|fn)"},
                    {"id":"85","title":"removeClass(class|fn)"},
                    {"id":"86","title":"toggleClass(class|fn[,sw])"},
                    {"id":"87","title":"html([val|fn])"},
                    {"id":"88","title":"text([val|fn])"},
                    {"id":"89","title":"val([val|fn|arr])"}
                ]},
                {"id":"90","title":"筛选","rename":"filter","child":[
                    {"id":"91","title":"eq(index|-index)"},
                    {"id":"92","title":"first()"},
                    {"id":"93","title":"last()"},
                    {"id":"94","title":"hasClass(class)"},
                    {"id":"95","title":"filter(expr|obj|ele|fn)"},
                    {"id":"96","title":"is(expr|obj|ele|fn)"},
                    {"id":"97","title":"map(callback)"},
                    {"id":"98","title":"has(expr|ele)"},
                    {"id":"99","title":"not(expr|ele)"},
                    {"id":"100","title":"slice(start,[end])"},
                    {"id":"101","title":"children([expr])"},
                    {"id":"102","title":"closest(expr,[con]|obj|ele)"},
                    {"id":"103","title":"find(expr|obj|ele)"},
                    {"id":"104","title":"next([expr])"},
                    {"id":"105","title":"nextAll([expr])"},
                    {"id":"106","title":"nextUntil([exp|ele][,fil])"},
                    {"id":"107","title":"parent([expr])"},
                    {"id":"108","title":"parents([expr])"},
                    {"id":"109","title":"parentsUntil([exp|ele][,fil])"},
                    {"id":"110","title":"prev([expr])"},
                    {"id":"111","title":"prevAll([expr])"},
                    {"id":"112","title":"prevUntil([exp|ele][,fil])"},
                    {"id":"113","title":"siblings([expr])"},
                    {"id":"114","title":"add(expr|ele|html|obj[,con])"},
                    {"id":"115","title":"andSelf()"},
                    {"id":"116","title":"contents()"},
                    {"id":"117","title":"end()"}
                ]},
                {"id":"118",title:"文档处理","rename":"content","child":[
                    {"id":"119","title":"append(content|fn)"},
                    {"id":"120","title":"appendTo(content)"},
                    {"id":"121","title":"prepend(content|fn)"},
                    {"id":"122","title":"prependTo(content)"},
                    {"id":"123","title":"after(content|fn)"},
                    {"id":"124","title":"before(content|fn)"},
                    {"id":"125","title":"insertAfter(content)"},
                    {"id":"126","title":"insertBefore(content)"},
                    {"id":"127","title":"wrap(html|ele|fn)"},
                    {"id":"128","title":"unwrap()"},
                    {"id":"129","title":"wrapAll(html|ele)"},
                    {"id":"130","title":"wrapInner(html|ele|fn)"},
                    {"id":"131","title":"replaceWith(content|fn)"},
                    {"id":"132","title":"replaceAll(selector)"},
                    {"id":"133","title":"empty()"},
                    {"id":"134","title":"remove([expr])"},
                    {"id":"135","title":"detach([expr])"},
                    {"id":"136","title":"clone([Even[,deepEven]])"}
                ]},
                {"id":"137","title":"CSS","rename":"CSS","child":[
                    {"id":"138","title":"css(name|pro|[,val|fn])"},
                    {"id":"139","title":"offset([coordinates])"},
                    {"id":"140","title":"position()"},
                    {"id":"141","title":"scrollTop([val])"},
                    {"id":"142","title":"scrollLeft([val])"},
                    {"id":"143","title":"height([val|fn])"},
                    {"id":"145","title":"width([val|fn])"},
                    {"id":"146","title":"innerHeight()"},
                    {"id":"147","title":"innerWidth()"},
                    {"id":"148","title":"outerHieght([options])"},
                    {"id":"149","title":"outerWidth([options])"}
                ]},
                {"id":"150","title":"事件","rename":"event","child":[
                    {"id":"151","title":"ready(fn)"},
                    {"id":"152","title":"on(events,[selector],[data],fn)"},
                    {"id":"153","title":"off(events,[selector],[fn])"},
                    {"id":"154","title":"bind(type, [data], fn)"},
                    {"id":"155","title":"one(type,[data],fn)"},
                    {"id":"156","title":"trigger(type,[data])"},
                    {"id":"157","title":"triggerHandler(type, [data])"},
                    {"id":"158","title":"unbind(type,[data|fn])"},
                    {"id":"159","title":"live(type, [data|fn])"},
                    {"id":"160","title":"die(type,[fn])"},
                    {"id":"161","title":"delegate(sel,[type],[data],fn)"},
                    {"id":"162","title":"undelegate([sel,[type],fn])"},
                    {"id":"163","title":"hover([over,]out)"},
                    {"id":"164","title":"toggle(fn,fn1[,fn2,fn3,...])"},
                    {"id":"165","title":"blur([[data],fn])"},
                    {"id":"166","title":"change([[data],fn])"},
                    {"id":"167","title":"click([[data],fn])"},
                    {"id":"168","title":"dblclick([[data],fn])"},
                    {"id":"169","title":"error([[data],fn])"},
                    {"id":"170","title":"focus([[data],fn])"},
                    {"id":"171","title":"focusin([[data],fn])"},
                    {"id":"172","title":"focusout([[data],fn])"},
                    {"id":"173","title":"keydown([[data],fn])"},
                    {"id":"174","title":"keypress([[data],fn])"},
                    {"id":"175","title":"keyup([[data],fn])"},
                    {"id":"176","title":"mousedown([[data],fn])"},
                    {"id":"177","title":"mouseenter([[data],fn])"},
                    {"id":"178","title":"mouseleave([[data],fn])"},
                    {"id":"179","title":"mousemove([[data],fn])"},
                    {"id":"180","title":"mouseout([[data],fn])"},
                    {"id":"181","title":"mouseover([[data],fn])"},
                    {"id":"182","title":"mouseup([[data],fn])"},
                    {"id":"183","title":"resize([[data],fn])"},
                    {"id":"184","title":"scroll([[data],fn])"},
                    {"id":"185","title":"select([[data],fn])"},
                    {"id":"186","title":"submit([[data],fn])"},
                    {"id":"187","title":"unload([[data],fn])"}
                ]},
                {"id":"188","title":"效果","rename":"animate","child":[
                    {"id":"189","title":"show([speed,[easing],[fn]])"},
                    {"id":"190","title":"hide([speed,[easing],[fn]])"},
                    {"id":"191","title":"toggle([speed],[easing],[fn])"},
                    {"id":"192","title":"slideDown([speed],[easing],[fn])"},
                    {"id":"193","title":"slideUp([speed,[easing],[fn]])"},
                    {"id":"194","title":"slideToggle([speed],[easing],[fn])"},
                    {"id":"195","title":"fadeIn([speed],[easing],[fn])"},
                    {"id":"196","title":"fadeOut([speed],[easing],[fn])"},
                    {"id":"197","title":"fadeTo([[speed],opacity,[easing],[fn]])"},
                    {"id":"198","title":"fadeToggle([speed,[easing],[fn]])"},
                    {"id":"199","title":"animate(param,[spe],[e],[fn])"},
                    {"id":"200","title":"stop([cle],[jum])"},
                    {"id":"201","title":"delay(duration,[queueName])"},
                    {"id":"202","title":"jQuery.fx.off"},
                    {"id":"203","title":"jQuery.fx.interval"}
                ]},
                {"id":"204","title":"Ajax","rename":"Ajax","child":[
                    {"id":"205","title":"jQuery.ajax(url,[settings])"},
                    {"id":"206","title":"load(url,[data],[callback])"},
                    {"id":"207","title":"jQuery.get(url,[data],[callback])"},
                    {"id":"208","title":"jQuery.getJSON(url,[data],[callback])"},
                    {"id":"209","title":"jQuery.getScript(url,[callback])"},
                    {"id":"210","title":"jQuery.post(url,[data],[callback])"},
                    {"id":"211","title":"ajaxComplete(callback)"},
                    {"id":"212","title":"ajaxError(callback)"},
                    {"id":"213","title":"ajaxSend(callback)"},
                    {"id":"214","title":"ajaxStart(callback)"},
                    {"id":"215","title":"ajaxStop(callback)"},
                    {"id":"216","title":"ajaxSuccess(callback)"},
                    {"id":"217","title":"jQuery.ajaxPrefilter([type],fn)"},
                    {"id":"218","title":"jQuery.ajaxSetup([options])"},
                    {"id":"219","title":"serialize()"},
                    {"id":"220","title":"serializeArray()"}
                ]},
                {"id":"221","title":"工具","rename":"tools","child":[
                    {"id":"222","title":"jQuery.support"},
                    {"id":"223","title":"jQuery.browser"},
                    {"id":"224","title":"jQuery.browser.version"},
                    {"id":"225","title":"jQuery.boxModel"},
                    {"id":"226","title":"jQuery.each(obj,[callback])"},
                    {"id":"227","title":"jQuery.extend([deep],target,object1,[objectN])"},
                    {"id":"228","title":"jQuery.grep(array,callback,[invert])"},
                    {"id":"229","title":"jQuery.makeArray(obj)"},
                    {"id":"230","title":"jQuery.map(array,callback)"},
                    {"id":"231","title":"jQuery.inArray(val,arr,[from])"},
                    {"id":"232","title":"jQuery.toArray()"},
                    {"id":"233","title":"jQuery.sub()"},
                    {"id":"234","title":"jQuery.when(deferreds)"},
                    {"id":"235","title":"jQuery.merge(first,second)"},
                    {"id":"236","title":"jQuery.unique(array)"},
                    {"id":"237","title":"jQuery.parseJSON(json)"},
                    {"id":"238","title":"jQuery.parseXML(data)"},
                    {"id":"239","title":"jQuery.noop"},
                    {"id":"240","title":"jQuery.proxy(function,context)"},
                    {"id":"241","title":"jQuery.contains(container,contained)"},
                    {"id":"242","title":"jQuery.isArray(obj)"},
                    {"id":"243","title":"jQuery.isFunction(obj)"},
                    {"id":"244","title":"jQuery.isEmptyObject(obj)"},
                    {"id":"245","title":"jQuery.isPlainObject(obj)"},
                    {"id":"246","title":"jQuery.isWindow(obj)"},
                    {"id":"247","title":"jQuery.isNumeric(value)"},
                    {"id":"248","title":"jQuery.type(obj)"},
                    {"id":"249","title":"jQuery.trim(str)"},
                    {"id":"250","title":"jQuery.param(obj,[traditional])"},
                    {"id":"251","title":"jQuery.error(message)"}
                ]},
                {"id":"252","title":"Event对象","rename":"eventObject","child":[
                    {"id":"253","title":"event.currentTarget"},
                    {"id":"254","title":"event.data"},
                    {"id":"255","title":"event.delegateTarget"},
                    {"id":"256","title":"event.isDefaultPrevented()"},
                    {"id":"257","title":"event.isImmediatePropagationStopped()"},
                    {"id":"258","title":"event.isPropagationStopped()"},
                    {"id":"259","title":"event.namespace"},
                    {"id":"260","title":"event.pageX"},
                    {"id":"261","title":"event.pageY"},
                    {"id":"262","title":"event.preventDefault()"},
                    {"id":"263","title":"event.relatedTarget"},
                    {"id":"264","title":"event.result"},
                    {"id":"265","title":"event.stopImmediatePropagation()"},
                    {"id":"266","title":"event.stopPropagation()"},
                    {"id":"267","title":"event.target"},
                    {"id":"268","title":"event.timeStamp"},
                    {"id":"269","title":"event.type"},
                    {"id":"270","title":"event.which"}
                ]},
                {"id":"271","title":"Deferred","rename":"Deferred","child":[
                    {"id":"272","title":"def.done(donCal,[donCal])"},
                    {"id":"273","title":"def.fail(failCal)"},
                    {"id":"274","title":"def.isRejected()"},
                    {"id":"275","title":"def.isResolved()"},
                    {"id":"276","title":"def.reject(args)"},
                    {"id":"277","title":"def.rejectWith(context,[args])"},
                    {"id":"278","title":"def.resolve(args)"},
                    {"id":"279","title":"def.resolveWith(context,args)"},
                    {"id":"280","title":"def.then(doneCal,failCal)"},
                    {"id":"281","title":"def.progress([type],[target])"},
                    {"id":"282","title":"def.pipe([donFil],[faiFil],[proFil])"},
                    {"id":"283","title":"def.always(alwCal,[alwCal])"},
                    {"id":"284","title":"def.notify(args)"},
                    {"id":"285","title":"def.notifyWidth(context,[args])"},
                    {"id":"286","title":"def.notify(args)"},
                    {"id":"287","title":"def.notifyWidth(context,[args])"},
                    {"id":"288","title":"def.progress(proCal)"},
                    {"id":"289","title":"def.state()"}
                ]},
                {"id":"290","title":"CallBacks","rename":"CallBacks","child":[
                    {"id":"291","title":"callbacks.add(callbacks)"},
                    {"id":"292","title":"callbacks.disable()"},
                    {"id":"293","title":"callbacks.empty()"},
                    {"id":"294","title":"callbacks.fire(arguments)"},
                    {"id":"295","title":"callbacks.fired()"},
                    {"id":"296","title":"callbacks.fireWith([context][,args])"},
                    {"id":"297","title":"callbacks.has(callback)"},
                    {"id":"298","title":"callbacks.lock()"},
                    {"id":"299","title":"callbacks.locked()"},
                    {"id":"300","title":"callbacks.remove(callbacks)"},
                    {"id":"301","title":"jQuery.callbacks(flags)"}
                ]},
                {"id":"302","title":"关于","rename":"about","child":[
                    {"id":"303","title":"关于jQuery API文档"},
                    {"id":"304","title":"提交BUG及获取更新"}
                ]},
                {"id":"305","title":"其他","rename":"others","child":[
                    {"id":"306","title":"CSS压缩/格式化"},
                    {"id":"307","title":"Js压缩/格式化"},
                    {"id":"308","title":"正则表达式在线测试"},
                    {"id":"309","title":"正则表达式速查表"},
                    {"id":"310","title":"HTML 5速查表"}
                ]}
            ],
            "content": {

            }
        },
        "callback": ""
    });
});

/* 获取文章菜单导航 */
router.get('/jQuery-1.83/:pselect',function(req, res){
    res.json({
        "statusCode": 200,
        "message": "请求信息成功",
        "data":{
            "crumbs": [
                {"title": "首页","href": "/"},
                {"title": "jQuery 1.83", "href":"/jQuery-1.83"},
                {"title": "属性", "href": "/jQuery-1.83/prototype"}
            ],
            "content": [
                {"id":"07","title":"jQuery(selector,[context])","rename":"jQuery_selector_context","supTip":"1.6+","isDelete":true,"summary":"<p>jQuery选择器是</p>"},
                {"id":"09","title":"jQuery(html,[ownerDocument])","rename": "jQuery_html_ownerDocument","supTip":"1.8*"},
                {"id":"10","title":"jQuery(callback)","rename":"jQuery_callback"},
                {"id":"11","title":"jQuery.holdReady(hold)","rename":"jQuery.holdReady","supTip": "1.6+"},
                {"id":"12","title":"each(callback)","rename":"each_callback"},
                {"id":"13","title":"size()","rename":"size"},
                {"id":"14","title":"length","rename":"length"},
                {"id":"15","title":"selector","rename":"selector"},
                {"id":"16","title":"context","rename":"context"},
                {"id":"17","title":"get([index])","rename":"get_index"},
                {"id":"18",'title':"index([selector|element])","rename":"index_selector_element"},
                {"id":"19","title":"data([key],[value])","rename":"data_key_value"},
                {"id":"20","title":"removeData([name|list])"},
                {"id":"21","title":"jQuery.data(element,[key],[value])"},
                {"id":"22","title":"queue(element,[queueName])"},
                {"id":"23","title":"dequeue([queueName])"},
                {"id":"24","title":"clearQueue([queueName])"},
                {"id":"25","title":"jQuery.noConflict([extreme])"}]
        }
    });
});

/*获取文章详情*/
router.get('/jQuery-1.83/core/:id',function(req, res){
    res.json({
        "statusCode":200,
        "message": "加载成功",
        "data":{
            "crumbs": [
                {"title": "首页","href": "/"},
                {"title": "jQuery 1.83", "href":"/jQuery-1.83"},
                {"title": "核心", "href": "/jQuery-1.83/core"},
                {"title": "jQuery(selector,[context])", "href": "/jQuery-1.83/core/jQuery_selector_context"}
            ],
            "content": {
                "title": "jQuery([selector,[context]])",
                "returnValue": "jQuery",
                "html":'<h3>概述</h3><div class="desc"><p>这个函数接收一个包含 CSS 选择器的字符串，然后用这个字符串去匹配一组元素。</p><div class="longdesc"><p>jQuery 的核心功能都是通过这个函数实现的。 jQuery中的一切都基于这个函数，或者说都是在以某种方式使用这个函数。这个函数最基本的用法就是向它传递一个表达式（通常由 CSS 选择器组成），然后根据这个表达式来查找所有匹配的元素。</p><p>默认情况下, 如果没有指定context参数，$()将在当前的 HTML document中查找 DOM 元素；如果指定了 context 参数，如一个 DOM 元素集或 jQuery 对象，那就会在这个 context 中查找。在jQuery 1.3.2以后，其返回的元素顺序等同于在context中出现的先后顺序。</p><p>参考文档中 选择器 部分获取更多用于 expression 参数的 CSS 语法的信息。</p></div></div><h3>参数</h3><div class="argument"><h4><strong>selector,[context]</strong><span>String,Element,/jQuery</span><em>V1.0</em></h4><p><strong>selector</strong>:用来查找的字符串</p><p><strong>context</strong>:作为待查找的 DOM 元素集、文档或 jQuery 对象。</p><h4><strong>element</strong><span>Element</span><em>V1.0</em></h4><p>一个用于封装成jQuery对象的DOM元素</p><h4><strong>object</strong><span>object</span><em>V1.0</em></h4><p>一个用于封装成jQuery对象</p><h4><strong>elementArray</strong><span>Element</span><em>V1.0</em></h4><p>一个用于封装成jQuery对象的DOM元素数组。</p><h4><strong>jQuery object</strong><span>object</span><em>V1.0</em></h4><p>一个用于克隆的jQuery对象。</p><h4><strong>jQuery()</strong><span></span><em>V1.4</em></h4><p>返回一个空的jQuery对象。</p></div><div class="example"><h3>示例</h3><span id="f_ad2"></span><h4>描述:</h4><p>找到所有 p 元素，并且这些元素都必须是 div 元素的子元素。</p><h5>jQuery 代码:</h5><pre class="prettyprint">$("div &gt; p");</pre><h4>描述:</h4><p>设置页面背景色。</p><h5>jQuery 代码:</h5><pre class="prettyprint">$(document.body).css( "background", "black" );</pre><h4>描述:</h4><p>隐藏一个表单中所有元素。</p><h5>jQuery 代码:</h5><pre class="prettyprint">$(myForm.elements).hide()</pre><h4>描述:</h4><p>在文档的第一个表单中，查找所有的单选按钮(即: type 值为 radio 的 input 元素)。</p><h5>jQuery 代码:</h5><pre class="prettyprint"><code>$("input:radio", document.forms[0]);</code></pre><h4>描述:</h4><p>在一个由 AJAX 返回的 XML 文档中，查找所有的 div 元素。</p><h5>jQuery 代码:</h5><pre class="prettyprint"><code>$("div", xml.responseXML);</code></pre></div>'
            }
        }
    });
});

module.exports = router;