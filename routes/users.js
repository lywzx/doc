var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});
router.get('/:id',function(req,res){
    res.send(req.params.id);
});
router.param('id', function (req, res, next, id) {
    console.log('CALLED ONLY ONCE');
    next();
});
module.exports = router;