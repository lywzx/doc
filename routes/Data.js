/**
 * Created with PhpStorm.
 * User: LiuYang
 * Date: 2014/11/22
 * Time: 15:46
 * Description:
 */


module.exports = [ {
    title:"核心",
    child:[ {
        title:"jQuery 核心函数",
        child:[ {
            title:"jQuery([sel,[context]])",
            href:"/book/jQuery/jQuery_selector_context.html",
            subTip:""
        }, {
            title:"jQuery(html,[ownerDoc])",
            href:"/book/jQuery/jQuery_html_ownerDocument.html",
            subTip:"1.8*"
        }, {
            title:"jQuery(callback)",
            href:"/book/jQuery/jQuery_callback.html",
            subTip:""
        }, {
            title:"jQuery.holdReady(hold)",
            href:"/book/jQuery/jQuery.holdReady.html",
            subTip:"1.6+"
        } ]
    }, {
        title:"jQuery 对象访问",
        child:[ {
            title:"each(callback)",
            href:"/book/jQuery/each.html",
            subTip:""
        }, {
            title:"size()",
            href:"/book/jQuery/size.html",
            subTip:""
        }, {
            title:"length",
            href:"/book/jQuery/length.html",
            subTip:""
        }, {
            title:"selector",
            href:"/book/jQuery/selector.html",
            subTip:""
        }, {
            title:"context",
            href:"/book/jQuery/context.html",
            subTip:""
        }, {
            title:"get([index])",
            href:"/book/jQuery/get.html",
            subTip:""
        }, {
            title:"index([selector|element])",
            href:"/book/jQuery/index1.html",
            subTip:""
        } ]
    }, {
        title:"数据缓存",
        child:[ {
            title:"data([key],[value])",
            href:"/book/jQuery/data.html",
            subTip:""
        }, {
            title:"removeData([name|list])",
            href:"/book/jQuery/removeData.html",
            subTip:"1.7*"
        }, {
            title:"$.data(ele,[key],[val])",
            href:"/book/jQuery/jQuery.data.html",
            subTip:"1.8-"
        } ]
    }, {
        title:"队列控制",
        child:[ {
            title:"queue(element,[queueName])",
            href:"/book/jQuery/queue.html",
            subTip:""
        }, {
            title:"dequeue([queueName])",
            href:"/book/jQuery/dequeue.html",
            subTip:""
        }, {
            title:"clearQueue([queueName])",
            href:"/book/jQuery/clearQueue.html",
            subTip:""
        } ]
    }, {
        title:"插件机制",
        child:[ {
            title:"jQuery.fn.extend(object)",
            href:"/book/jQuery/jQuery.fn.extend.html",
            subTip:""
        }, {
            title:"jQuery.extend(object)",
            href:"/book/jQuery/jQuery.extend_object.html",
            subTip:""
        } ]
    }, {
        title:"多库共存",
        child:[ {
            title:"jQuery.noConflict([ex])",
            href:"/book/jQuery/jQuery.noConflict.html",
            subTip:""
        } ]
    } ]
}, {
    title:"属性",
    child:[ {
        title:"属性",
        child:[ {
            title:"attr(name|pro|key,val|fn)",
            href:"/book/jQuery/attr.html",
            subTip:""
        }, {
            title:"removeAttr(name)",
            href:"/book/jQuery/removeAttr.html",
            subTip:""
        }, {
            title:"prop(name|pro|key,val|fn)",
            href:"/book/jQuery/prop.html",
            subTip:"1.6+"
        }, {
            title:"removeProp(name)",
            href:"/book/jQuery/removeProp.html",
            subTip:"1.6+"
        } ]
    }, {
        title:"CSS 类",
        child:[ {
            title:"addClass(class|fn)",
            href:"/book/jQuery/addClass.html",
            subTip:""
        }, {
            title:"removeClass([class|fn])",
            href:"/book/jQuery/removeClass.html",
            subTip:""
        }, {
            title:"toggleClass(class|fn[,sw])",
            href:"/book/jQuery/toggleClass.html",
            subTip:""
        } ]
    }, {
        title:"HTML代码/文本/值",
        child:[ {
            title:"html([val|fn])",
            href:"/book/jQuery/html.html",
            subTip:""
        }, {
            title:"text([val|fn])",
            href:"/book/jQuery/text.html",
            subTip:""
        }, {
            title:"val([val|fn|arr])",
            href:"/book/jQuery/val.html",
            subTip:""
        } ]
    } ]
}, {
    title:"CSS",
    child:[ {
        title:"CSS",
        child:[ {
            title:"css(name|pro|[,val|fn])",
            href:"/book/jQuery/css.html",
            subTip:"1.8*"
        } ]
    }, {
        title:"位置",
        child:[ {
            title:"offset([coordinates])",
            href:"/book/jQuery/offset.html",
            subTip:""
        }, {
            title:"position()",
            href:"/book/jQuery/position.html",
            subTip:""
        }, {
            title:"scrollTop([val])",
            href:"/book/jQuery/scrollTop.html",
            subTip:""
        }, {
            title:"scrollLeft([val])",
            href:"/book/jQuery/scrollLeft.html",
            subTip:""
        } ]
    }, {
        title:"尺寸",
        child:[ {
            title:"heigh([val|fn])",
            href:"/book/jQuery/height.html",
            subTip:""
        }, {
            title:"width([val|fn])",
            href:"/book/jQuery/width.html",
            subTip:""
        }, {
            title:"innerHeight()",
            href:"/book/jQuery/innerHeight.html",
            subTip:""
        }, {
            title:"innerWidth()",
            href:"/book/jQuery/innerWidth.html",
            subTip:""
        }, {
            title:"outerHeight([soptions])",
            href:"/book/jQuery/outerHeight.html",
            subTip:""
        }, {
            title:"outerWidth([options])",
            href:"/book/jQuery/outerWidth.html",
            subTip:""
        } ]
    } ]
}, {
    title:"选择器",
    child:[ {
        title:"基本",
        child:[ {
            title:"#id",
            href:"/book/jQuery/id.html",
            subTip:""
        }, {
            title:"element",
            href:"/book/jQuery/element.html",
            subTip:""
        }, {
            title:".class",
            href:"/book/jQuery/class.html",
            subTip:""
        }, {
            title:"*",
            href:"/book/jQuery/all.html",
            subTip:""
        }, {
            title:"selector1,selector2,selectorN",
            href:"/book/jQuery/multiple.html",
            subTip:""
        } ]
    }, {
        title:"层级",
        child:[ {
            title:"ancestor descendant",
            href:"/book/jQuery/descendant.html",
            subTip:""
        }, {
            title:"parent >  child",
            href:"/book/jQuery/child.html",
            subTip:""
        }, {
            title:"prev + next",
            href:"/book/jQuery/next_1.html",
            subTip:""
        }, {
            title:"prev ~ siblings",
            href:"/book/jQuery/siblings_1.html",
            subTip:""
        } ]
    }, {
        title:"基本",
        child:[ {
            title:":first",
            href:"/book/jQuery/first_1.html",
            subTip:""
        }, {
            title:":last",
            href:"/book/jQuery/last_1.html",
            subTip:""
        }, {
            title:":not(selector)",
            href:"/book/jQuery/not_1.html",
            subTip:""
        }, {
            title:":even",
            href:"/book/jQuery/even.html",
            subTip:""
        }, {
            title:":odd",
            href:"/book/jQuery/odd.html",
            subTip:""
        }, {
            title:":eq(index)",
            href:"/book/jQuery/eq_1.html",
            subTip:""
        }, {
            title:":gt(index)",
            href:"/book/jQuery/gt.html",
            subTip:""
        }, {
            title:":lt(index)",
            href:"/book/jQuery/lt.html",
            subTip:""
        }, {
            title:":header",
            href:"/book/jQuery/header.html",
            subTip:""
        }, {
            title:":animated",
            href:"/book/jQuery/animated.html",
            subTip:""
        }, {
            title:":focus",
            href:"/book/jQuery/focus_1.html",
            subTip:"1.6+"
        } ]
    }, {
        title:"内容",
        child:[ {
            title:":contains(text)",
            href:"/book/jQuery/contains.html",
            subTip:""
        }, {
            title:":empty",
            href:"/book/jQuery/empty_1.html",
            subTip:""
        }, {
            title:":has(selector)",
            href:"/book/jQuery/has_1.html",
            subTip:""
        }, {
            title:":parent",
            href:"/book/jQuery/parent_1.html",
            subTip:""
        } ]
    }, {
        title:"可见性",
        child:[ {
            title:":hidden",
            href:"/book/jQuery/hidden_1.html",
            subTip:""
        }, {
            title:":visible",
            href:"/book/jQuery/visible.html",
            subTip:""
        } ]
    }, {
        title:"属性",
        child:[ {
            title:"[attribute]",
            href:"/book/jQuery/attributeHas.html",
            subTip:""
        }, {
            title:"[attribute=value]",
            href:"/book/jQuery/attributeEquals.html",
            subTip:""
        }, {
            title:"[attribute!=value]",
            href:"/book/jQuery/attributeNotEqual.html",
            subTip:""
        }, {
            title:"[attribute^=value]",
            href:"/book/jQuery/attributeStartsWith.html",
            subTip:""
        }, {
            title:"[attribute$=value]",
            href:"/book/jQuery/attributeEndsWith.html",
            subTip:""
        }, {
            title:"[attribute*=value]",
            href:"/book/jQuery/attributeContains.html",
            subTip:""
        }, {
            title:"[attrSel1][attrSel2][attrSelN]",
            href:"/book/jQuery/attributeMultiple.html",
            subTip:""
        } ]
    }, {
        title:"子元素",
        child:[ {
            title:":nth-child",
            href:"/book/jQuery/nthChild.html",
            subTip:""
        }, {
            title:":first-child",
            href:"/book/jQuery/firstChild.html",
            subTip:""
        }, {
            title:":last-child",
            href:"/book/jQuery/lastChild.html",
            subTip:""
        }, {
            title:":only-child",
            href:"/book/jQuery/onlyChild.html",
            subTip:""
        } ]
    }, {
        title:"表单",
        child:[ {
            title:":input",
            href:"/book/jQuery/input.html",
            subTip:""
        }, {
            title:":text",
            href:"/book/jQuery/text_1.html",
            subTip:""
        }, {
            title:":password",
            href:"/book/jQuery/password.html",
            subTip:""
        }, {
            title:":radio",
            href:"/book/jQuery/radio.html",
            subTip:""
        }, {
            title:":checkbox",
            href:"/book/jQuery/checkbox.html",
            subTip:""
        }, {
            title:":submit",
            href:"/book/jQuery/submit_1.html",
            subTip:""
        }, {
            title:":image",
            href:"/book/jQuery/image.html",
            subTip:""
        }, {
            title:":reset",
            href:"/book/jQuery/reset.html",
            subTip:""
        }, {
            title:":button",
            href:"/book/jQuery/button.html",
            subTip:""
        }, {
            title:":file",
            href:"/book/jQuery/file.html",
            subTip:""
        }, {
            title:":hidden",
            href:"/book/jQuery/hidden_1.html",
            subTip:""
        } ]
    }, {
        title:"表单对象属性",
        child:[ {
            title:":enabled",
            href:"/book/jQuery/enabled.html",
            subTip:""
        }, {
            title:":disabled",
            href:"/book/jQuery/disabled.html",
            subTip:""
        }, {
            title:":checked",
            href:"/book/jQuery/checked.html",
            subTip:""
        }, {
            title:":selected",
            href:"/book/jQuery/selected.html",
            subTip:""
        } ]
    } ]
}, {
    title:"文档处理",
    child:[ {
        title:"内部插入",
        child:[ {
            title:"append(content|fn)",
            href:"/book/jQuery/append.html",
            subTip:""
        }, {
            title:"appendTo(content)",
            href:"/book/jQuery/appendTo.html",
            subTip:""
        }, {
            title:"prepend(content|fn)",
            href:"/book/jQuery/prepend.html",
            subTip:""
        }, {
            title:"prependTo(content)",
            href:"/book/jQuery/prependTo.html",
            subTip:""
        } ]
    }, {
        title:"外部插入",
        child:[ {
            title:"after(content|fn)",
            href:"/book/jQuery/after.html",
            subTip:""
        }, {
            title:"before(content|fn)",
            href:"/book/jQuery/before.html",
            subTip:""
        }, {
            title:"insertAfter(content)",
            href:"/book/jQuery/insertAfter.html",
            subTip:""
        }, {
            title:"insertBefore(content)",
            href:"/book/jQuery/insertBefore.html",
            subTip:""
        } ]
    }, {
        title:"包裹",
        child:[ {
            title:"wrap(html|ele|fn)",
            href:"/book/jQuery/wrap.html",
            subTip:""
        }, {
            title:"unwrap()",
            href:"/book/jQuery/unwrap.html",
            subTip:""
        }, {
            title:"wrapall(html|ele)",
            href:"/book/jQuery/wrapAll.html",
            subTip:""
        }, {
            title:"wrapInner(html|ele|fn)",
            href:"/book/jQuery/wrapInner.html",
            subTip:""
        } ]
    }, {
        title:"替换",
        child:[ {
            title:"replaceWith(content|fn)",
            href:"/book/jQuery/replaceWith.html",
            subTip:""
        }, {
            title:"replaceAll(selector)",
            href:"/book/jQuery/replaceAll.html",
            subTip:""
        } ]
    }, {
        title:"删除",
        child:[ {
            title:"empty()",
            href:"/book/jQuery/empty.html",
            subTip:""
        }, {
            title:"remove([expr])",
            href:"/book/jQuery/remove.html",
            subTip:""
        }, {
            title:"detach([expr])",
            href:"/book/jQuery/detach.html",
            subTip:""
        } ]
    }, {
        title:"复制",
        child:[ {
            title:"clone([Even[,deepEven]])",
            href:"/book/jQuery/clone.html",
            subTip:""
        } ]
    } ]
}, {
    title:"筛选",
    child:[ {
        title:"过滤",
        child:[ {
            title:"eq(index|-index)",
            href:"/book/jQuery/eq.html",
            subTip:""
        }, {
            title:"first()",
            href:"/book/jQuery/first.html",
            subTip:""
        }, {
            title:"last()",
            href:"/book/jQuery/last.html",
            subTip:""
        }, {
            title:"hasClass(class)",
            href:"/book/jQuery/hasClass.html",
            subTip:""
        }, {
            title:"filter(expr|obj|ele|fn)",
            href:"/book/jQuery/filter.html",
            subTip:""
        }, {
            title:"is(expr|obj|ele|fn)",
            href:"/book/jQuery/is.html",
            subTip:"1.6*"
        }, {
            title:"map(callback)",
            href:"/book/jQuery/map.html",
            subTip:""
        }, {
            title:"has(expr|ele)",
            href:"/book/jQuery/has.html",
            subTip:""
        }, {
            title:"not(expr|ele|fn)",
            href:"/book/jQuery/not.html",
            subTip:""
        }, {
            title:"slice(start,[end])",
            href:"/book/jQuery/slice.html",
            subTip:""
        } ]
    }, {
        title:"查找",
        child:[ {
            title:"children([expr])",
            href:"/book/jQuery/children.html",
            subTip:""
        }, {
            title:"closest(expr,[con]|obj|ele)",
            href:"/book/jQuery/closest.html",
            subTip:"1.6*"
        }, {
            title:"find(expr|obj|ele)",
            href:"/book/jQuery/find.html",
            subTip:"1.6*"
        }, {
            title:"next([expr])",
            href:"/book/jQuery/next.html",
            subTip:""
        }, {
            title:"nextall([expr])",
            href:"/book/jQuery/nextAll.html",
            subTip:""
        }, {
            title:"nextUntil([exp|ele][,fil])",
            href:"/book/jQuery/nextUntil.html",
            subTip:"1.6*"
        }, {
            title:"offsetParent()",
            href:"/book/jQuery/offsetParent.html",
            subTip:""
        }, {
            title:"parent([expr])",
            href:"/book/jQuery/parent.html",
            subTip:""
        }, {
            title:"parents([expr])",
            href:"/book/jQuery/parents.html",
            subTip:""
        }, {
            title:"parentsUntil([exp|ele][,fil])",
            href:"/book/jQuery/parentsUntil.html",
            subTip:"1.6*"
        }, {
            title:"prev([expr])",
            href:"/book/jQuery/prev.html",
            subTip:""
        }, {
            title:"prevall([expr])",
            href:"/book/jQuery/prevAll.html",
            subTip:""
        }, {
            title:"prevUntil([exp|ele][,fil])",
            href:"/book/jQuery/prevUntil.html",
            subTip:"1.6*"
        }, {
            title:"siblings([expr])",
            href:"/book/jQuery/siblings.html",
            subTip:""
        } ]
    }, {
        title:"串联",
        child:[ {
            title:"add(expr|ele|html|obj[,con])",
            href:"/book/jQuery/add.html",
            subTip:""
        }, {
            title:"andSelf()",
            href:"/book/jQuery/andSelf.html",
            subTip:""
        }, {
            title:"contents()",
            href:"/book/jQuery/contents.html",
            subTip:""
        }, {
            title:"end()",
            href:"/book/jQuery/end.html",
            subTip:""
        } ]
    } ]
}, {
    title:"事件",
    child:[ {
        title:"页面载入",
        child:[ {
            title:"ready(fn)",
            href:"/book/jQuery/ready.html",
            subTip:""
        } ]
    }, {
        title:"事件处理",
        child:[ {
            title:"on(eve,[sel],[data],fn)",
            href:"/book/jQuery/on.html",
            subTip:"1.7+"
        }, {
            title:"off(eve,[sel],[fn])",
            href:"/book/jQuery/off.html",
            subTip:"1.7+"
        }, {
            title:"bind(type,[data],fn)",
            href:"/book/jQuery/bind.html",
            subTip:""
        }, {
            title:"one(type,[data],fn)",
            href:"/book/jQuery/one.html",
            subTip:""
        }, {
            title:"trigger(type,[data])",
            href:"/book/jQuery/trigger.html",
            subTip:""
        }, {
            title:"triggerHandler(type, [data])",
            href:"/book/jQuery/triggerHandler.html",
            subTip:""
        }, {
            title:"unbind(type,[data|fn])",
            href:"/book/jQuery/unbind.html",
            subTip:""
        } ]
    }, {
        title:"事件委派",
        child:[ {
            title:"live(type,[data],fn)",
            href:"/book/jQuery/live.html",
            subTip:""
        }, {
            title:"die(type,[fn])",
            href:"/book/jQuery/die.html",
            subTip:""
        }, {
            title:"delegate(sel,[type],[data],fn) ",
            href:"/book/jQuery/delegate.html",
            subTip:""
        }, {
            title:"undelegate([sel,[type],fn]) ",
            href:"/book/jQuery/undelegate.html",
            subTip:"1.6*"
        } ]
    }, {
        title:"事件切换",
        child:[ {
            title:"hover([over,]out)",
            href:"/book/jQuery/hover.html",
            subTip:""
        }, {
            title:"toggle(fn, fn2, [fn3, fn4, ...])",
            href:"/book/jQuery/toggle.html",
            subTip:""
        } ]
    }, {
        title:"事件",
        child:[ {
            title:"blur([[data],fn])",
            href:"/book/jQuery/blur.html",
            subTip:""
        }, {
            title:"change([[data],fn])",
            href:"/book/jQuery/change.html",
            subTip:""
        }, {
            title:"click([[data],fn])",
            href:"/book/jQuery/click.html",
            subTip:""
        }, {
            title:"dblclick([[data],fn])",
            href:"/book/jQuery/dblclick.html",
            subTip:""
        }, {
            title:"error([[data],fn])",
            href:"/book/jQuery/error.html",
            subTip:""
        }, {
            title:"focus([[data],fn])",
            href:"/book/jQuery/focus.html",
            subTip:""
        }, {
            title:"focusin([data],fn)",
            href:"/book/jQuery/focusin.html",
            subTip:""
        }, {
            title:"focusout([data],fn)",
            href:"/book/jQuery/focusout.html",
            subTip:""
        }, {
            title:"keydown([[data],fn])",
            href:"/book/jQuery/keydown.html",
            subTip:""
        }, {
            title:"keypress([[data],fn])",
            href:"/book/jQuery/keypress.html",
            subTip:""
        }, {
            title:"keyup([[data],fn])",
            href:"/book/jQuery/keyup.html",
            subTip:""
        }, {
            title:"mousedown([[data],fn])",
            href:"/book/jQuery/mousedown.html",
            subTip:""
        }, {
            title:"mouseenter([[data],fn])",
            href:"/book/jQuery/mouseenter.html",
            subTip:""
        }, {
            title:"mouseleave([[data],fn])",
            href:"/book/jQuery/mouseleave.html",
            subTip:""
        }, {
            title:"mousemove([[data],fn])",
            href:"/book/jQuery/mousemove.html",
            subTip:""
        }, {
            title:"mouseout([[data],fn])",
            href:"/book/jQuery/mouseout.html",
            subTip:""
        }, {
            title:"mouseover([[data],fn])",
            href:"/book/jQuery/mouseover.html",
            subTip:""
        }, {
            title:"mouseup([[data],fn])",
            href:"/book/jQuery/mouseup.html",
            subTip:""
        }, {
            title:"resize([[data],fn])",
            href:"/book/jQuery/resize.html",
            subTip:""
        }, {
            title:"scroll([[data],fn])",
            href:"/book/jQuery/scroll.html",
            subTip:""
        }, {
            title:"select([[data],fn])",
            href:"/book/jQuery/select.html",
            subTip:""
        }, {
            title:"submit([[data],fn])",
            href:"/book/jQuery/submit.html",
            subTip:""
        }, {
            title:"unload([[data],fn])",
            href:"/book/jQuery/unload.html",
            subTip:""
        } ]
    } ]
}, {
    title:"效果",
    child:[ {
        title:"基本",
        child:[ {
            title:"show([speed,[easing],[fn]])",
            href:"/book/jQuery/show.html",
            subTip:""
        }, {
            title:"hide([speed,[easing],[fn]])",
            href:"/book/jQuery/hide.html",
            subTip:""
        }, {
            title:"toggle([speed],[easing],[fn])",
            href:"/book/jQuery/toggle.html",
            subTip:""
        } ]
    }, {
        title:"滑动",
        child:[ {
            title:"slideDown([spe],[eas],[fn])",
            href:"/book/jQuery/slideDown.html",
            subTip:""
        }, {
            title:"slideUp([speed,[easing],[fn]])",
            href:"/book/jQuery/slideUp.html",
            subTip:""
        }, {
            title:"slideToggle([speed],[easing],[fn])",
            href:"/book/jQuery/slideToggle.html",
            subTip:""
        } ]
    }, {
        title:"淡入淡出",
        child:[ {
            title:"fadeIn([speed],[eas],[fn])",
            href:"/book/jQuery/fadeIn.html",
            subTip:""
        }, {
            title:"fadeOut([speed],[eas],[fn])",
            href:"/book/jQuery/fadeOut.html",
            subTip:""
        }, {
            title:"fadeTo([[spe],opa,[eas],[fn]])",
            href:"/book/jQuery/fadeTo.html",
            subTip:""
        }, {
            title:"fadeToggle([speed,[eas],[fn]])",
            href:"/book/jQuery/fadeToggle.html",
            subTip:""
        } ]
    }, {
        title:"自定义",
        child:[ {
            title:"animate(par,[spe],[e],[fn])",
            href:"/book/jQuery/animate.html",
            subTip:"1.8*"
        }, {
            title:"stop([cle],[jum])",
            href:"/book/jQuery/stop.html",
            subTip:"1.7*"
        }, {
            title:"delay(duration,[queueName])",
            href:"/book/jQuery/delay.html",
            subTip:""
        } ]
    }, {
        title:"设置",
        child:[ {
            title:"jQuery.fx.off",
            href:"/book/jQuery/jQuery.fx.off.html",
            subTip:""
        }, {
            title:"jQuery.fx.interval",
            href:"/book/jQuery/jQuery.fx.interval.html",
            subTip:""
        } ]
    } ]
}, {
    title:"ajax",
    child:[ {
        title:"ajax 请求",
        child:[ {
            title:"$.ajax(url,[settings])",
            href:"/book/jQuery/jQuery.ajax.html",
            subTip:""
        }, {
            title:"load(url,[data],[callback])",
            href:"/book/jQuery/load.html",
            subTip:""
        }, {
            title:"$.get(url,[data],[fn],[type])",
            href:"/book/jQuery/jQuery.get.html",
            subTip:""
        }, {
            title:"$.getJSON(url,[data],[fn])",
            href:"/book/jQuery/jQuery.getJSON.html",
            subTip:""
        }, {
            title:"$.getScript(url,[callback])",
            href:"/book/jQuery/jQuery.getScript.html",
            subTip:""
        }, {
            title:"$.post(url,[data],[fn],[type])",
            href:"/book/jQuery/jQuery.post.html",
            subTip:""
        } ]
    }, {
        title:"ajax 事件",
        child:[ {
            title:"ajaxComplete(callback)",
            href:"/book/jQuery/ajaxComplete.html",
            subTip:""
        }, {
            title:"ajaxError(callback)",
            href:"/book/jQuery/ajaxError.html",
            subTip:""
        }, {
            title:"ajaxSend(callback)",
            href:"/book/jQuery/ajaxSend.html",
            subTip:""
        }, {
            title:"ajaxStart(callback)",
            href:"/book/jQuery/ajaxStart.html",
            subTip:""
        }, {
            title:"ajaxStop(callback)",
            href:"/book/jQuery/ajaxStop.html",
            subTip:""
        }, {
            title:"ajaxSuccess(callback)",
            href:"/book/jQuery/ajaxSuccess.html",
            subTip:""
        } ]
    }, {
        title:"其它",
        child:[ {
            title:"$.ajaxSetup([options])",
            href:"/book/jQuery/jQuery.ajaxSetup.html",
            subTip:""
        }, {
            title:"serialize()",
            href:"/book/jQuery/serialize.html",
            subTip:""
        }, {
            title:"serializearray()",
            href:"/book/jQuery/serializearray.html",
            subTip:""
        } ]
    } ]
}, {
    title:"工具",
    child:[ {
        title:"浏览器及特性检测",
        child:[ {
            title:"$.support",
            href:"/book/jQuery/jQuery.support.html",
            subTip:""
        }, {
            title:"$.browser",
            href:"/book/jQuery/jQuery.browser.html",
            subTip:""
        }, {
            title:"$.browser.version",
            href:"/book/jQuery/jQuery.browser.version.html",
            subTip:""
        }, {
            title:"$.boxModel",
            href:"/book/jQuery/jQuery.boxModel.html",
            subTip:""
        } ]
    }, {
        title:"数组和对象操作",
        child:[ {
            title:"$.each(object,[callback])",
            href:"/book/jQuery/jQuery.each.html",
            subTip:""
        }, {
            title:"$.extend([d],tgt,obj1,[objN])",
            href:"/book/jQuery/jQuery.extend.html",
            subTip:""
        }, {
            title:"$.grep(array,fn,[invert])",
            href:"/book/jQuery/jQuery.grep.html",
            subTip:""
        }, {
            title:"$.sub()",
            href:"/book/jQuery/jQuery.sub.html",
            subTip:""
        }, {
            title:"$.when(deferreds)",
            href:"/book/jQuery/jQuery.when.html",
            subTip:""
        }, {
            title:"$.makearray(obj)",
            href:"/book/jQuery/jQuery.makeArray.html",
            subTip:""
        }, {
            title:"$.map(arr|obj,callback)",
            href:"/book/jQuery/jQuery.map.html",
            subTip:"1.6*"
        }, {
            title:"$.inarray(val,arr,[from])",
            href:"/book/jQuery/jQuery.inArray.html",
            subTip:""
        }, {
            title:"$.toarray()",
            href:"/book/jQuery/jQuery.toarray.html",
            subTip:""
        }, {
            title:"$.merge(first,second)",
            href:"/book/jQuery/jQuery.merge.html",
            subTip:""
        }, {
            title:"$.unique(array)",
            href:"/book/jQuery/jQuery.unique.html",
            subTip:""
        }, {
            title:"$.parseJSON(json)",
            href:"/book/jQuery/jQuery.parseJSON.html",
            subTip:""
        } ]
    }, {
        title:"函数操作",
        child:[ {
            title:"$.noop",
            href:"/book/jQuery/jQuery.noop.html",
            subTip:""
        }, {
            title:"$.proxy(function,context)",
            href:"/book/jQuery/jQuery.proxy.html",
            subTip:""
        } ]
    }, {
        title:"测试操作",
        child:[ {
            title:"$.contains(container,contained)",
            href:"/book/jQuery/jQuery.contains.html",
            subTip:""
        }, {
            title:"$.type(obj)",
            href:"/book/jQuery/jQuery.type.html",
            subTip:""
        }, {
            title:"$.isarray(obj)",
            href:"/book/jQuery/jQuery.isArray.html",
            subTip:""
        }, {
            title:"$.isFunction(obj)",
            href:"/book/jQuery/jQuery.isFunction.html",
            subTip:""
        }, {
            title:"$.isEmptyObject(obj)",
            href:"/book/jQuery/jQuery.isEmptyObject.html",
            subTip:""
        }, {
            title:"$.isPlainObject(obj)",
            href:"/book/jQuery/jQuery.isPlainObject.html",
            subTip:""
        }, {
            title:"$.isWindow(obj)",
            href:"/book/jQuery/jQuery.isWindow.html",
            subTip:""
        }, {
            title:"$.isNumeric(value)",
            href:"/book/jQuery/jQuery.isNumeric.html",
            subTip:"1.7+"
        } ]
    }, {
        title:"字符串操作",
        child:[ {
            title:"$.trim(str)",
            href:"/book/jQuery/jQuery.trim.html",
            subTip:""
        } ]
    }, {
        title:"URL",
        child:[ {
            title:"$.param(obj,[traditional])",
            href:"/book/jQuery/jQuery.param.html",
            subTip:""
        } ]
    }, {
        title:"插件编写",
        child:[ {
            title:"$.error(message)",
            href:"/book/jQuery/jQuery.error.html",
            subTip:""
        } ]
    } ]
}, {
    title:"关于",
    child:[ {
        title:"关于此jQuery中文文档",
        href:"/book/jQuery/about.html",
        child:[]
    }, {
        title:"提交bug及获取更新",
        href:"/book/jQuery/bugandUpdate.html",
        child:[]
    } ]
}, {
    title:"Deferred",
    child:[ {
        title:"def.done(donCal,[donCal])",
        href:"/book/jQuery/deferred.done.html",
        child:[]
    }, {
        title:"def.fail(failCallbacks)",
        href:"/book/jQuery/deferred.fail.html",
        child:[]
    }, {
        title:"",
        child:[]
    }, {
        title:"",
        child:[]
    }, {
        title:"def.reject(args)",
        href:"/book/jQuery/deferred.reject.html",
        child:[]
    }, {
        title:"def.rejectWith(context,[args])",
        href:"/book/jQuery/deferred.rejectWith.html",
        child:[]
    }, {
        title:"def.resolve(args)",
        href:"/book/jQuery/deferred.resolve.html",
        child:[]
    }, {
        title:"def.resolveWith(context,[args])",
        href:"/book/jQuery/deferred.resolveWith.html",
        child:[]
    }, {
        title:"def.then(doneCal,failCals)",
        href:"/book/jQuery/deferred.then.html",
        child:[]
    }, {
        title:"def.promise([type],[target])",
        href:"/book/jQuery/deferred.promise.html",
        child:[]
    }, {
        title:"def.pipe([donl],[fai],[pro])",
        href:"/book/jQuery/deferred.pipe.html",
        child:[]
    }, {
        title:"def.always(alwCal,[alwCal])",
        href:"/book/jQuery/deferred.always.html",
        child:[]
    }, {
        title:"def.notify(args)",
        href:"/book/jQuery/deferred.notify.html",
        child:[]
    }, {
        title:"def.notifyWith(con,[args])",
        href:"/book/jQuery/deferred.notifyWith.html",
        child:[]
    }, {
        title:"def.progress(proCal)",
        href:"/book/jQuery/deferred.progress.html",
        child:[]
    }, {
        title:"def.state()",
        href:"/book/jQuery/deferred.state.html",
        child:[]
    } ]
}, {
    title:"Callbacks",
    child:[ {
        title:"cal.add(callbacks)",
        href:"/book/jQuery/callbacks.add.html",
        child:[]
    }, {
        title:"cal.disable()",
        href:"/book/jQuery/callbacks.disable.html",
        child:[]
    }, {
        title:"cal.empty()",
        href:"/book/jQuery/callbacks.empty.html",
        child:[]
    }, {
        title:"cal.fire(arguments)",
        href:"/book/jQuery/callbacks.fire.html",
        child:[]
    }, {
        title:"cal.fired()",
        href:"/book/jQuery/callbacks.fired.html",
        child:[]
    }, {
        title:"cal.fireWith([context] [, args])",
        href:"/book/jQuery/callbacks.fireWith.html",
        child:[]
    }, {
        title:"cal.has(callback)",
        href:"/book/jQuery/callbacks.has.html",
        child:[]
    }, {
        title:"cal.lock()",
        href:"/book/jQuery/callbacks.lock.html",
        child:[]
    }, {
        title:"cal.locked()",
        href:"/book/jQuery/callbacks.locked.html",
        child:[]
    }, {
        title:"cal.remove(callbacks)",
        href:"/book/jQuery/callbacks.remove.html",
        child:[]
    }, {
        title:"$.callbacks(flags)",
        href:"/book/jQuery/jQuery.callbacks.html",
        child:[]
    } ]
} ];