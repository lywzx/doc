#  相关说明
## 系统整体概述
  前端基于AngularJs, RequireJs实现的帮助系统，后端基于ThinkPHP实现，数据分两种类型获取，json或jsonp的方式获取。
#  全局API说明

>返回参数说明

    "statusCode": 200,   //信息返回状态码,200:表示请求成功; 404:页面不存在错误; 500:服务器错误; 0: 操作失败
    "message": "操作成功"  //返回的信息提示信息


## 前端路由格式如下：`/:type/:pselect/:id`
* `type`表示文档类别（如：jQuery-1.7.8）
* `pselect`表示文档内部分类
* `id`表示对应文档对应ID

>网站的首页相关

    //

>帮助文档首页

    //帮助系统网站首页接口
    url : "/json/"
    method: "get"
    requestParams: {
        "type": "all"
    },
    responseData: {
        "statusCode": 200,
        "message": "首页内容获取成功",
        "data" : [
            {"id": 01, "title": "前端系列帮助手册", "rename": "web", "keywords": "web,前端", "summary": "前端系列帮助手册主要包含CSS系列与JavaScript系列手册", "picurl": "www.lyblog.net", "count": 10
                "handbook": [
                    {"id": 03, "title": "jQuery 1.83中文帮助手册", "rename": "jQuery1.83", "kwywords": "javascript,jQuery", "summary": "jQuery 1.83简介"，"picurl": "www.lyblog.net", "sort": 10},
                    {"id": 04, "title": "CSS 3帮助手册", "rename": "webkitcss3", "kwywords": "webkit,css3", "summary": "WEBKIT浏览器帮助手册"，"picurl": "www.lyblog.net"},
                    ...
                ]
            },
            {"id": 05, "title": "PHP参考手册", "rename": "php5", "keywords": "php5,php帮助手册", "summary": "前端系列帮助手册主要包含CSS系列与JavaScript系列手册", "picurl": "www.lyblog.net", "count": 10
                "handbook":[]
            }
            ...
        ]
    }

>帮助文档列表页相关接口

    //文档右侧导航内容




>帮助文档正文相关接口

    //文档右侧导航及文档
    url:'/ajax/jquery',
    method: 'get',
    requestParams: {
        type: jQuery-1.83
    },
    responseData: {
        "statusCode": 200,
        "message": "数据请求成功",
        "data": [
            {"id": 203, "title": "速查表", "keywords": "关键字", "rename": "quicksearch",
                "archive": [
                    {"id": 208, "title": "", "rename": "jQuery","keywords": "关键字", "supTip": "1.6+","isDelete": true},
                    ...
                ]
            },
            {"id": 204, "title": "核心", "title": "关键字", "rename": "core"},
            ...
        ]
    }


    //获取某个二级栏目的文档内容
    url: "/ajax/jquery/core"
    method: 'get',
    requestParams: {
        type: "jquery",
        pselect: "core"
    }
    responseData: {
        "statusCode": 200,
        "message": "数据请求成功",
        "data": {
            "title": "标题",
            "summary": "描述内容",
            "list": [
                {"id": 10, "title": "jQuery.fn.fix","keywords": "jquery,fix","summary": "简单描述","supTip": "1.6+", "isDelete": true}
            ]
        }
    }
